# Manuela Silva <mmsrs@sky.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: paprefs\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pulseaudio/paprefs/"
"issues/new\n"
"POT-Creation-Date: 2020-02-13 09:37+0200\n"
"PO-Revision-Date: 2020-05-27 13:40+0000\n"
"Last-Translator: Manuela Silva <mmsrs@sky.com>\n"
"Language-Team: Portuguese <https://translate.fedoraproject.org/projects/"
"pulseaudio/paprefs/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.0.4\n"
"X-Poedit-Language: Portuguese\n"
"X-Poedit-Country: PORTUGAL\n"

#: src/paprefs.desktop.in:5 src/paprefs.glade:7
msgid "PulseAudio Preferences"
msgstr "Preferências do PulseAudio"

#: src/paprefs.desktop.in:6
msgid "Sound Server Preferences"
msgstr "Preferências do Servidor de Som"

#: src/paprefs.desktop.in:7
msgid "View and modify the configuration of the local sound server"
msgstr "Veja e altere a configuração do servidor de som local"

#: src/paprefs.desktop.in:9
msgid "preferences-desktop"
msgstr ""

#: src/paprefs.glade:33
msgid "Make discoverable _PulseAudio network sound devices available locally"
msgstr ""
"_Disponibilizar os dispositivos de som _PulseAudio detetáveis localmente"

#: src/paprefs.glade:49 src/paprefs.glade:91 src/paprefs.glade:168
#: src/paprefs.glade:219 src/paprefs.glade:285 src/paprefs.glade:381
#: src/paprefs.glade:422
msgid "Install..."
msgstr "Instalar..."

#: src/paprefs.glade:75
msgid "Make discoverable Apple A_irTunes sound devices available locally"
msgstr ""
"Disponibilizar os dispositivos de som Apple Air_tunes detetáveis localmente"

#: src/paprefs.glade:116
msgid ""
"<i>Apple and AirTunes are trademarks of Apple Inc., registered in the U.S. "
"and other countries.</i>"
msgstr ""
"<i>Apple e Airtunes são marcas registadas da Apple Inc., registadas nos EUA "
"e em outros países.</i>"

#: src/paprefs.glade:134
msgid "Network _Access"
msgstr "Acesso de Rede"

#: src/paprefs.glade:153
msgid "Enable _network access to local sound devices"
msgstr "Activar o acesso à _rede pelos dispositivos de som locais"

#: src/paprefs.glade:204
msgid "Allow other machines on the LAN to _discover local sound devices"
msgstr ""
"Permitir que outras máquinas na rede _encontrem dispositivos de som locais"

#: src/paprefs.glade:241
msgid "Don't _require authentication"
msgstr "Não _requer autenticação"

#: src/paprefs.glade:269
msgid "Make local sound devices available as DLNA/_UPnP Media Server"
msgstr ""
"Disponibilizar os dispositivos de som locais como Servidor de Multimédia "
"DLNA/_UPnP"

#: src/paprefs.glade:316
msgid "Create separate audio device for DLNA/UPnP media streaming"
msgstr ""
"Criar um _dispositivo de áudio distinto para transmissão de multimédia DLNA/"
"UPnP"

#: src/paprefs.glade:346
msgid "Network _Server"
msgstr "Servidor de Rede"

#: src/paprefs.glade:366
msgid "Enable Multicast/RTP re_ceiver"
msgstr "Ativar re_cetor Multicast/RTP"

#: src/paprefs.glade:407
msgid "Enable Multicast/RTP s_ender"
msgstr "Ativar _emissor de Multicast/RTP"

#: src/paprefs.glade:454
msgid "Send audio from local _microphone"
msgstr "Enviar áudio a partir do _microfone local"

#: src/paprefs.glade:470
msgid "Send audio from local spea_kers"
msgstr "Enviar áudio a partir das c_olunas locais"

#: src/paprefs.glade:486
msgid "Create separate audio device _for Multicast/RTP"
msgstr "Criar um _dispositivo de áudio distinto para Multicast/RTP"

#: src/paprefs.glade:502
msgid "_Loop back audio to local speakers"
msgstr "_Repetir áudio para as coluna locais"

#: src/paprefs.glade:517
msgid "Send on fixed _port 5004"
msgstr ""

#: src/paprefs.glade:546
msgid "Multicast/R_TP"
msgstr "Multicast/R_TP"

#: src/paprefs.glade:562
msgid ""
"Add _virtual output device for simultaneous output on all local sound cards"
msgstr ""
"Adicionar um dispositivo _virtual de saída para saídas simultâneas em todas "
"as placas de som locais"

#: src/paprefs.glade:581
msgid "Simultaneous _Output"
msgstr "Saída Sim_ultânea"

#~ msgid ""
#~ "<span color=\"black\">View and modify the configuration of your local "
#~ "sound server</span>"
#~ msgstr ""
#~ "<span color=\"black\">Veja e altere a configuração do servidor de som "
#~ "local</span>"

#~ msgid ""
#~ "<span size=\"18000\" color=\"black\"><b>PulseAudio Preferences</b></span>"
#~ msgstr ""
#~ "<span size=\"18000\" color=\"black\"><b>Preferências PulseAudio</b></span>"
